# Poll Aggregation in Real Time (PART) #

* The app provides an interface for conducting polls/quizzes/surveys and obtaining the results in real time.
* A user who wishes to conduct polls can create an account using a verified email-id.
* S/He can create classrooms and add quizzes to it which will contain objective questions with multiple choice. A classroom can be used by more than one user which enables people to collaborate on projects . 
* The user can publish and close the quiz as and when required which makes it possible to use it as a fun social quiz app or for testing purposes .
* Students can answer quizzes/surveys anonymously.
* The classrooms,quizzes,votes etc.. are stored in the database and can be accessed by the user anytime.


### Technology requirements ###

* Django 1.10
* Postgresql 9.6.1
* Python 3.5.2
* Django/Python packages : 
 django-registration
 websockets
 asyncio


### Who do I talk to? ###

* Group-77 :
Vaibhav Nagar
Tushant Mittal
Pranay Borkar